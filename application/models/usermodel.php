<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserModel extends CI_model {
	
public function __construct() {	
		parent::__construct();
}
	
	public function insert_an_user($data){
			$this->db->insert('usuarios', $data);
			return  TRUE;

	}


	public function login($usuario, $pass){

		$rs=$this->db->query("select * from usuarios where nombre='".$usuario."' and  password='".$pass."'");
		return $rs->num_rows();
	}

	public function info($usuario, $pass){

		$rs=$this->db->query("select * from usuarios where nombre='".$usuario."' and  password='".$pass."'");
		return $rs->num_rows() != 0 ? $rs->result() : NULL;

	}

	public function token_exist($id, $usuario){

		$rs=$this->db->query("select * from usuarios where nombre='".$usuario."' and  usuario_id='".$id."'");
		return $rs->num_rows() != 0 ? true : false;

	}


	public function mis_gatos($usuario){

		$rs=$this->db->query("select * from gatos where usuario_id='".$usuario."'");
		return $rs->num_rows() != 0 ? $rs->result() : NULL;

	}

	public function insert_a_cat($data){
			$rs=$this->db->insert('gatos', $data);
			return  true ;

	}

	public function mis_viajes($usuario){

		$rs=$this->db->query("select * from viajes inner join gatos on gatos.gato_id=viajes.gato_id where viajes.usuario_id='".$usuario."'");
		return $rs->num_rows() != 0 ? $rs->result() : NULL;

	}

	public function insert_a_trip($data){
			$rs=$this->db->insert('viajes', $data);
			return  true ;

	}


	public function mis_comidas($usuario){

		$rs=$this->db->query("select * from comida inner join gatos on gatos.gato_id=comida.gato_id where comida.usuario_id='".$usuario."'");
		return $rs->num_rows() != 0 ? $rs->result() : NULL;

	}

	public function insert_a_food($data){
			$rs=$this->db->insert('comida', $data);
			return  true ;
	}


	public function kill_a_cat($id){
		$rs=$this->db->query("delete from gatos where gato_id='".$id."'");
		return true;
	}


	public function kill_a_trip($id){
		$rs=$this->db->query("delete from viajes where viaje_id='".$id."'");
		return true;
	}


	public function kill_a_food($id){
		$rs=$this->db->query("delete from comida where comida_id='".$id."'");
		return true;
	}

	public function update_a_cat($id,$data){
			$this->db->where('gato_id', $id);
			$this->db->update('gatos', $data);
			return true;
	}

	public function update_a_trip($id,$data){
			$this->db->where('viaje_id', $id);
			$this->db->update('viajes', $data);
			return true;
	}

	public function update_a_food($id,$data){
			$this->db->where('comida_id', $id);
			$this->db->update('comida', $data);
			return true;
	}


	

	
}