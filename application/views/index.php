<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="http://dtai.uteq.edu.mx/~cruosv188/imagenes/web/favico.ico" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js">
	</script>
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous">

	<title>Gatos_viajes.com</title>


	<script>
		var base_url = "<?= base_url() ?>";
	</script>
	<script src="<?= base_url() ?>static/js/scriptos.js"></script>

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #870d85;">
		<a class="navbar-brand" href="#">
  <!--<img src="http://dtai.uteq.edu.mx/~cruosv188/imagenes/neolerning.png" style="height:100% ">
  -->
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
	<div class="container-fluid">
		<div class="row mb-12">
			<img src="http://dtai.uteq.edu.mx/~cruosv188/imagenes/web/log7.png" style=" width: 10%">
		</div>
		<div class="row mb-12">

			<div class="col-sm-6 form-group" align="center">
				<a href="#" data-toggle="modal" data-target="#log" class="text-white"><i class="fas fa-sign-out-alt" style="font-size: 30px"></i><br>Acceder</a>
			</div>
			<div class="col-sm-6 form-group" align="center">
				<a href="#" data-toggle="modal" data-target="#Reg" class="text-white"><i class="fas fa-user-plus" style="font-size: 30px"></i><br>Registrase</a>
			</div>
		</div>

	</div>
</div>
</nav>
	
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="http://www.mascotahogar.com/Imagenes/dibujo-de-un-gato-como-wallpaper.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
    <h5>Registra tus viajes</h5>
    <p>Registrate para conocer mas </p>
  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://fondosmil.com/fondo/22137.jpg" alt="Second slide">
      <div class="carousel-caption d-none d-md-block">
    <h5>Registra fotos de tus gatos</h5>
    <p>Conoce mas </p>
  </div>
    </div>
    
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>
<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Mas informacion</h5>
        <p>Entregable de seguridad<br>
        Sitio de uso gratuito para la clase de seguridad
        <br>
        Es gratis y siempre lo sera.
        </p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase"></h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!"></a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Links</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!"><i class="fab fa-instagram"></i>Instagram</a>
          </li>
          <li>
            <a href="#!"><i class="fab fa-facebook-f"></i>Facebook</a>
          </li>
          <li>
            <a href="#!"><i class="fab fa-twitter"></i>Twitter</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2021 Copyright:
    <a href="#">Gatos&viajes.com</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
<!--SALSA DE LOGIN-->
<div class="modal fade" id="log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel"><i class="fas fa-user"></i>Iniciar Sesion</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

			</div>
			<div class="modal-body">
				<div class="container">
						<div class="jumbotron col-sm-12">

							<div class="row">
								<div class="card-body">
								
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-user"></i></span>
											</div>
											<input type="text" class="form-control" placeholder="Usuario" id="log_user">

										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-key"></i></span>
											</div>
											<input type="password" class="form-control" placeholder="Contraseña" id="log_pass">
										</div>
										<div class="form-group">
											<button class="btn float-right login_btn btn-success" id="btn-login">Entrar</button>
										
										</div>
								</div>
								<div class="card-footer">
									<div class="d-flex justify-content-center links">
										No tienes una cuenta? <a href="#">Registrate</a>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--fin de login-->
<!---salsa de Registro-->
<div class="modal fade" id="Reg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel"><i class="fas fa-user-plus"></i>Registro</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

			</div>
			<div class="modal-body">
				<div class="container">
						<div class="jumbotron col-sm-12">

							<div class="row">
								<div class="card-body">
						
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-user"></i></span>
											</div>
											<input type="text" class="form-control" placeholder="ingresa Usuario" id="crea_user"required="true">

										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-key"></i></span>
											</div>
											<input type="password" class="form-control" placeholder="Crear Contraseña" id="crea_pass"
											required="true">
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-envelope"></i></span> 
											</div>
											<input type="mail" class="form-control" placeholder="Correo" id="mail" required="true">
										</div>
										<div class="form-group">	
											<button class="btn float-right login_btn btn-success" id="btn-crear">Registrate</button>
										</div>
						
								</div>
								<div class="card-footer">
									<div class="d-flex justify-content-center links">
										Ya tienes una cuenta? <a href="#" id="log"  data-toggle="modal">Inicia Sesion</a>
									</div>
									
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!---fin de registro -->
</body>
</html>