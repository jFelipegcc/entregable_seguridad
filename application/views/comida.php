<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Comida/gatos.com</title>

  <!-- Custom fonts for this template-->
  <link href="https://localhost/seguridad/static/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"> </script>
  <link rel="stylesheet" type="text/css" href="https://localhost/seguridad/static/css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="shortcut icon" href="http://dtai.uteq.edu.mx/~cruosv188/imagenes/web/favico.ico" />
  <!-- Custom styles for this template-->
  <link href="https://localhost/seguridad/static/css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/grad.css">

  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous">
</head>

<body id="page-top">

  <!-- Page Wrapper -->


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid ">

          <!-- Page Heading -->
         
          <div class="container">
            <div class="row">
              <div class="col-12 my-3 pt-3 shadow">
                
          
            <div class="card">
                <h3 class="card-header colorText" >Agregar Comida</h3>
              <div class="card-body"style="margin: 10px">

                  <div class="container ">

              <div class="row">
                  <div class="card">

                  <div class="d-flex justify-content-center links">

                    <div class="scrolling-wrapper row flex-row container-fluid flex-nowrap mt-12 pb-12">
                    </div>
                  </div>
                </div>
                <div class="card-body overflow-auto">
                
                    <div class="input-group form-group">
                        <label for="Text" class="col-sm-4 col-form-label">Que comiste: </label>
                        
                          <input type="text" class="form-control" placeholder="Comida" id="comida">
                        
                    </div>


                    <div class="input-group form-group">
                        <label for="Text" class="col-sm-4 col-form-label">Que comio el gato: </label>
                        
                          <input type="text" class="form-control" placeholder="Comida del gato" id="comida_gato">
                        
                    </div>
                
                    
                
                    
                    
                    <div class="input-group form-group">
                        <label for="Text" class="col-sm-4 col-form-label">Gato con el que comiste</label>
                        
                          <select id="partner">
                            <?php  foreach ( $gatos as $gato ) {
                                    $a = (object) $gato; ?>
                              <option value="<?= $a->gato_id  ?>"><?= $a->nombre_gato  ?></option>
                            <?php } ?>
                          </select>

                          <input id="gato_viaje" type="hidden" >

                          <br>
                      <button class="btn float-left btn-success" id='save-food'>Guardar Comida</button>


                        
                    </div>
                                    
                    <div class="form-group">
                    
                 <button class="btn float-left btn-success" onclick='
        location.href = "https://localhost/seguridad/usercontroller/home"'>Mis gatos</button>
                         <button class="btn float-left btn-primary" onclick='
        location.href = "https://localhost/seguridad/usercontroller/viajes"'>Mis viajes</button>
                      <button class="btn float-left btn-danger" onclick='
        location.href = "https://localhost/seguridad/usercontroller/comida"'>Mi comida</button>
                         
                    </div>
                </div>




                
              </div>

            </div>
            <hr>

      <hr>
      <table class="table table-hover">
  <thead>
    <tr class="text-white bg-dark">
      <th>Comida</th>
      <th>Comida del gato</th>
      <th>Gato</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
  <?php
  if ( $comidas != NULL ) :
    foreach ( $comidas as $gato ) :
      $a = (object) $gato;
  ?>
    <tr>
      <td><input type="text" class="form-control" id="comida<?=$a->comida_id?>" value="<?= base64_decode($a->comida) ?>"></td>

      <td><input type="text" class="form-control" id="comida_gato<?=$a->comida_id?>" value="<?= base64_decode($a->comida_gato) ?>"></td> 
      <td>
          <select id="partner<?=$a->comida_id?>">

            <option selected="select" value="<?= $a->gato_id  ?>"><?= $a->nombre_gato  ?></option>
            <?php  foreach ( $gatos as $gato ) {
                    $b = (object) $gato; ?>
              <option value="<?= $b->gato_id  ?>"><?= $b->nombre_gato  ?></option>
            <?php } ?>
          </select>

          <td>
        <button type="button" class="btn btn-primary btn-actualizar"
          onclick="update_food('<?= $a->comida_id ?>')">
          <i class="fas fa-edit"></i>
        </button>

        <button type="button" class="btn btn-danger btn-borrar"
          

          onclick="kill_food('<?=$a->comida_id?>')"
        >

          <i class="fas fa-trash"></i>
        </button>
      
      </td>
    </tr>
  <?php
    endforeach;
  endif;
  ?>
  </tbody>
</table>

                   
              </div>
            </div>
        </div>
           

</div>

      <!-- Footer -->
       <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Neolearning 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 
  </div>
</div>
  
  <!-- Bootstrap core JavaScript-->
  <script src="https://localhost/seguridad/vendor/jquery/jquery.min.js"></script>
  <script src="https://localhost/seguridad/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="https://localhost/seguridad/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="https://localhost/seguridad/js/sb-admin-2.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
  
    <script>
    var base_url = "<?= base_url() ?>";
  </script>
  <script src="<?= base_url() ?>static/js/scriptos.js"></script>



</body>

</html>
