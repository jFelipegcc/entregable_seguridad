<?php
	 
require_once APPPATH .'libraries\jwt\vendor/autoload.php';
use Firebase\JWT\JWT;
defined('BASEPATH') OR exit('No direct script access allowed');


class UserController extends CI_Controller {


	function __construct() {
		parent::__construct();
		$this->load->model('usermodel');
		$this->load->library('session');
		$this->secret='clavesecreta';
	header( "Access-Control-Allow-Origin: *" ); 
	header( "Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With" );

	}
	 
	function index() {
 		$this->load->view("index.php");
	}


	public function login(){

	$usuario = $this->input->post( "usuario" ); 
	$pass = md5($this->input->post( "contraseña" )); //la contraseña se encripta utilizando md5
		$rows = $this->usermodel->login($usuario,$pass); 
		if ($rows>0) {
			$cosas= $this->usermodel->info($usuario,$pass);
			$data["status"]=true;

			$time = time();
			$key = $this->secret;

			$token = array(
			'iat' => $time, // Tiempo que inició el token
			'exp' => $time + (60*60), // Tiempo que expirará el token (+1 hora)
			'data' => [ // información del usuario
			    'id' => $cosas[0]->usuario_id,
			    'name' => $cosas[0]->nombre
			]
			);

			$jwt = JWT::encode($token, $key); //encriptamos nuestro token
			$info = array('usuario' => $usuario, 'logged'=>true, 'token'=>$jwt );
			$this->session->set_userdata($info);


		}else{
			$data["status"]=false;
		}


		$this->output->set_content_type( "application/json" );
		
		echo json_encode($info);	
	}

	function ver(){
		$token = $this->session->userdata('token');
		try{
			
			$data = JWT::decode($token, $this->secret, array('HS256'));
			
			echo "Token:    ". $token ;

			echo "<br>desencriptado:	";
			var_dump($data);
		
		}catch(Exception $e){

			$this->session->sess_destroy();
			redirect('usercontroller'); 
		}
	}


	//altas
	function insert_an_user(){
		$usuario = $this->input->post( "usuario" );
		$pass =md5($this->input->post( "pass" ));
		$mail = base64_encode($this->input->post( "mail" ));
		$data=['nombre'=>$usuario, 'password'=>$pass, 'email'=>$mail];	
		$this->usermodel->insert_an_user($data);
		$this->output->set_content_type( "application/json" );
		echo json_encode( $data );	
	}

	function insert_a_cat(){
		$token = $this->session->userdata('token');
		try{
			
			$data = JWT::decode($token, $this->secret, array('HS256'));
			$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
		
		}catch(Exception $e){

			$this->session->sess_destroy();
			redirect('usercontroller'); 
		}
		
		if($exist){
			$gato = $this->input->post( "gato" );
			$color =base64_encode($this->input->post( "color" ));
			$raza =base64_encode($this->input->post( "raza" ));

			$data2=['nombre_gato'=>$gato, 'color_gato'=>$color, 'raza_gato'=>$raza, 'usuario_id'=>$data->data->id];	
			$final=$this->usermodel->insert_a_cat($data2);
			if($final){
				$salida = array('code' => '200','mensaje' => 'Insertado con exito'  );
			}else{

				$salida = array('code' => '400','mensaje' => 'Error'  );
			}
			$this->output->set_content_type( "application/json" );
			echo json_encode( $salida );	
		}
	}

	function insert_a_trip(){
		$token = $this->session->userdata('token');
		try{
			
			$data = JWT::decode($token, $this->secret, array('HS256'));
			$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
		
		}catch(Exception $e){

			$this->session->sess_destroy();
			redirect('usercontroller'); 
		}
		
		if($exist){
			$gato = $this->input->post( "gato" );
			$lugar =base64_encode($this->input->post( "lugar" ));
			$fecha =$this->input->post( "fecha" );

			$data2=['gato_id'=>$gato, 'fecha_viaje'=>$fecha, 'lugar'=>$lugar, 'usuario_id'=>$data->data->id];	
			$final=$this->usermodel->insert_a_trip($data2);
			if($final){
				$salida = array('code' => '200','mensaje' => 'Insertado con exito'  );
			}else{

				$salida = array('code' => '400','mensaje' => 'Error'  );
			}
			$this->output->set_content_type( "application/json" );
			echo json_encode( $salida );	
		}
	}


	function insert_a_food(){
		$token = $this->session->userdata('token');
		try{
			
			$data = JWT::decode($token, $this->secret, array('HS256'));
			$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
		
		}catch(Exception $e){

			$this->session->sess_destroy();
			redirect('usercontroller'); 
		}
		
		if($exist){
			$gato = $this->input->post( "gato" );
			$comida =base64_encode($this->input->post( "comida" ));
			$comida_gato =base64_encode($this->input->post( "comida_gato" ));

			$data2=['gato_id'=>$gato, 'comida'=>$comida, 'comida_gato'=>$comida_gato, 'usuario_id'=>$data->data->id];	
			$final=$this->usermodel->insert_a_food($data2);
			if($final){
				$salida = array('code' => '200','mensaje' => 'Insertado con exito'  );
			}else{

				$salida = array('code' => '400','mensaje' => 'Error'  );
			}
			$this->output->set_content_type( "application/json" );
			echo json_encode( $salida );	
		}
	}


	//Navegacion
	public function home(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){
			$token = $this->session->userdata('token');
			try{
				
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			
			if($exist){

			$salida['gatos']=$this->usermodel->mis_gatos($data->data->id);		
			$this->load->view('gatos',$salida);

			}else{

				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}

	public function comida(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){
			$token = $this->session->userdata('token');
			try{
				
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			
			if($exist){

			$salida['gatos']=$this->usermodel->mis_gatos($data->data->id);	
			$salida['comidas']=$this->usermodel->mis_comidas($data->data->id);		
			$this->load->view('comida',$salida);

			}else{

				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}

	public function viajes(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){
			$token = $this->session->userdata('token');
			try{
				
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			
			if($exist){

			$salida['viajes']=$this->usermodel->mis_viajes($data->data->id);	
			$salida['gatos']=$this->usermodel->mis_gatos($data->data->id);		
			$this->load->view('viajes',$salida);

			}else{

				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}


	//deletes


	public function kill_a_cat(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){
			$token = $this->session->userdata('token');
			try{
				
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			if($exist){
				$id = $this->input->post("id"); 
				$final=$this->usermodel->kill_a_cat($id);

				if($final){
					$salida = array('code' => '200','mensaje' => 'Eliminado con exito'  );
				}else{
					$salida = array('code' => '400','mensaje' => 'Error'  );
				}

				$this->output->set_content_type( "application/json" );
				echo json_encode( $salida );


			}else{
				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}


	public function kill_a_trip(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){
			$token = $this->session->userdata('token');
			try{
				
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			if($exist){
				$id = $this->input->post("id"); 
				$final=$this->usermodel->kill_a_trip($id);

				if($final){
					$salida = array('code' => '200','mensaje' => 'Eliminado con exito'  );
				}else{
					$salida = array('code' => '400','mensaje' => 'Error'  );
				}

				$this->output->set_content_type( "application/json" );
				echo json_encode( $salida );


			}else{
				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}


	public function kill_a_food(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){
			$token = $this->session->userdata('token');
			try{
				
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			if($exist){
				$id = $this->input->post("id"); 
				$final=$this->usermodel->kill_a_food($id);

				if($final){
					$salida = array('code' => '200','mensaje' => 'Eliminado con exito'  );
				}else{
					$salida = array('code' => '400','mensaje' => 'Error'  );
				}

				$this->output->set_content_type( "application/json" );
				echo json_encode( $salida );


			}else{
				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}

	//Update
	public function update_a_cat(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){

			$token = $this->session->userdata('token');

			try{
			
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			if($exist){
				$id = $this->input->post("id");
				$gato = $this->input->post("gato");
				$color = base64_encode($this->input->post("color"));
				$raza = base64_encode($this->input->post("raza")); 
				$data2=['nombre_gato'=>$gato, 'color_gato'=>$color, 'raza_gato'=>$raza];
				$final=$this->usermodel->update_a_cat($id,$data2);
				if($final){
					$salida = array('code' => '200','mensaje' => 'Actualizado con exito'  );
				}else{
					$salida = array('code' => '400','mensaje' => 'Error'  );
				}

				$this->output->set_content_type( "application/json" );
				echo json_encode( $salida );


			}else{
				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}

	public function update_a_trip(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){

			$token = $this->session->userdata('token');

			try{
			
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			if($exist){
				$id = $this->input->post("id");
				$gato = $this->input->post("gato");
				$lugar = base64_encode($this->input->post("lugar"));
				$fecha = $this->input->post("fecha");
				
				$data2=['fecha_viaje'=>$fecha, 'lugar'=>$lugar, 'gato_id'=>$gato];
				$final=$this->usermodel->update_a_trip($id,$data2);
				if($final){
					$salida = array('code' => '200','mensaje' => 'Actualizado con exito'  );
				}else{
					$salida = array('code' => '400','mensaje' => 'Error'  );
				}

				$this->output->set_content_type( "application/json" );
				echo json_encode( $salida );


			}else{
				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}

	public function update_a_food(){
		$is_logged=$this->session->userdata('logged');
		if($is_logged){

			$token = $this->session->userdata('token');

			try{
			
				$data = JWT::decode($token, $this->secret, array('HS256'));
				$exist=$this->usermodel->token_exist($data->data->id,$data->data->name);
			
			}catch(Exception $e){

				$this->session->sess_destroy();
				redirect('usercontroller'); 
			}
			if($exist){
				$id = $this->input->post("id"); //id de la comida
				$gato = $this->input->post("gato");
				$comida = base64_encode($this->input->post("comida"));
				$comida_gato = base64_encode($this->input->post("comida_gato"));
				
				$data2=['comida'=>$comida, 'comida_gato'=>$comida_gato,'usuario_id'=>$data->data->id,'gato_id'=>$gato ];
				$final=$this->usermodel->update_a_food($id,$data2);
				if($final){
					$salida = array('code' => '200','mensaje' => 'Actualizado con exito'  );
				}else{
					$salida = array('code' => '400','mensaje' => 'Error'  );
				}

				$this->output->set_content_type( "application/json" );
				echo json_encode( $salida );


			}else{
				redirect('usercontroller');
			}

		}else{
			redirect('usercontroller');
		}
	}



	
}