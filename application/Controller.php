<?php
require_once APPPATH .'libraries\jwt\vendor/autoload.php';
use Firebase\JWT\JWT;
class Controller extends CI_Controller {
public function __construct() {

	parent::__construct();
	$this->load->model( "Model" );
		
	header( "Access-Control-Allow-Origin: *" ); 
	header( "Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With" );

}


public function index(){
	$this->load->view('index.php');
}

public function login(){ 
// en esta funcion debemos de mandar el usuario y pass por post
//En caso de que el modelo valide dicho login pasamos a crear el token


//1.- recibes por post

//2.- validas que existan 

//3.- si existen: creamos el token y lo guardamos del lado del cliente


$time = time();
$key = 'secretosecreto';

$token = array(
    'iat' => $time, // Tiempo que inició el token
    'exp' => $time + (60*60), // Tiempo que expirará el token (+1 hora)
    'data' => [ // información del usuario
        'id' => 1,
        'name' => 'Juan'
    ]
);

$jwt = JWT::encode($token, $key);
$data = JWT::decode($jwt, $key, array('HS256'));

//var_dump($jwt);
$data['token']=$jwt;
$this->load->view('crud', $data);


}


}