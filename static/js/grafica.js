function click_grafica($usuario) {
		
	$.ajax({
		"url"      : "http://localhost/neolearning/usercontroller/grafica/"+$usuario,
		"dataType" : "json",
		"success"  : function( obj ) {



Highcharts.chart('grafica', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Amigos y reacciones acumuladas'
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Cantidad de personas'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: "Seguidores y reacciones",
            colorByPoint: true,
            data: [
                {
                    name: "Amigos",
                    y: obj.seguidores,
                },
                {
                    name: "Reacciones",
                    y: obj.likes,
                }
            ]
        }
    ],
  
});
}
	});
}
